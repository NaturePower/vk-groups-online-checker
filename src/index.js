const VkontakteAPI = require('./api-wrappers/vkontakte-api-wrapper.js');
const {pushDataToFile} = require('./utils/pushDataToFile.js');
const {filterUsers} = require('./utils/filterUsers.js');
const {convertData} = require('./utils/convertData.js');
const {convertReportData} = require('./utils/convertReportData.js');
const cron = require('node-cron');

let onlineUsers = []

const reports = {
  'bots_markets': [
    'botmarketru',
    'enigmarobotics',
    'softhut',
    'bots_market',
    'softwarerental',
    'cmd_root',
    'pasichniy_private',
    'retail_plus',
    'snkrlinks',
    'quasarcook',
    'copalerts',
    'drooptoop',
    'alliancecooks',
    'thunder.cloud'
  ]
}


cron.schedule('0 */28 * * * *', () => {
  getStatistic('bots_markets')
});

cron.schedule('30 */28 * * * *', () => {
  getStatistic('cooks')
});

async function getStatistic(reportName) {
  onlineUsers = []

  for (groupID of reports[reportName]) {
    await VkontakteAPI.getGroupUsers(groupID).then((data) => {
      pushDataToFile(groupID, convertData(data))

      onlineUsers = filterUsers(onlineUsers, data)
    })
  }

  pushDataToFile(`REPORT-${reportName}`, convertReportData(onlineUsers))
}