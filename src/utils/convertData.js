module.exports.convertData = function convertData(data) {
  let onlineUsersCount = 0

  let now = new Date();
  let hours = now.getTimezoneOffset() === -180 ? now.getHours() : (now.getHours() + 3)

  let tickTime = hours + ':' + now.getMinutes()

  data.forEach(user => {
    if ((user.online === 1)) onlineUsersCount += 1
  })

  return {
    "time": tickTime,
    "online": onlineUsersCount
  }
}
