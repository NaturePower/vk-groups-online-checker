module.exports.filterUsers = function filterUsers(onlineUsers, data) {
  data.forEach(user=>{
    if ((user.online === 1) && (onlineUsers.findIndex((userID) => userID === user.id) === -1)) {
      onlineUsers.push(user.id)
    }
  })

  return onlineUsers

}