const xlsx = require('node-xlsx').default;
const fs = require('fs');

const templatePath = `template.xlsx`

module.exports.pushDataToFile = function pushDataToFile(fileName, data) {
  let filePath = `excel_files/${fileName}.xlsx`
  let fileBuffer;

  try {
    fileBuffer = fs.readFileSync(filePath)
  } catch {
    fileBuffer = fs.readFileSync(templatePath)
  }

  let workSheetsFromBuffer = xlsx.parse(fileBuffer);

  workSheetsFromBuffer[0].name = fileName
  workSheetsFromBuffer[0].data.push([
    data.time, data.online
  ])
  
  fs.writeFileSync(filePath, xlsx.build(workSheetsFromBuffer))
}


