module.exports.convertReportData = function convertReportData(data) {
  let now = new Date();
  let hours = now.getTimezoneOffset() === -180 ? now.getHours() : (now.getHours() + 3)

  let tickTime = hours + ':' + now.getMinutes()

  return {
    "time": tickTime,
    "online": data.length
  }
}