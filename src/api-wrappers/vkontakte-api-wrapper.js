const axios = require('axios');

const token = "eb0a0e30eb0a0e30eb0a0e3006eb7927bbeeb0aeb0a0e30b41376748b63ccdb8353aab8"


module.exports.getGroupUsers = function getGroupUsers(group) {
  let url = `https://api.vk.com/method/groups.getMembers?group_id=${group}&fields=online&v=5.52&access_token=${token}`

    return new Promise((resolve) => {
      axios({
        method: 'get',
        url: url
      }).then(resp => {
        resolve(resp.data.response.items)
      })
    })
}